package pl.codementors.pracadomowa1;

/**
 * Homework task 4-11.
 * <p>
 * Specific animal group - Reptiles.
 *
 * @author Damir
 */

public abstract class Reptiles extends Animals {
    /**
     * Reptiles colour.
     */
    private String colourScales;

    //private fields available by Getters and Setters

    public String getColourScales() {
        return colourScales;
    }

    public void setColourScales(String colourScales) {
        this.colourScales = colourScales;
    }
}
