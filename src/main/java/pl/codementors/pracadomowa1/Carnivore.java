package pl.codementors.pracadomowa1;

/**
 * Homework task 4-11.
 *
 * Interface which will be implemented by things that can be cast.
 * @author Damir
 */

public interface Carnivore {

    void eatMeat();
}
