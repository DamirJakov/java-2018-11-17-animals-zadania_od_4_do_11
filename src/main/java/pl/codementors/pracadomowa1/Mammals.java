package pl.codementors.pracadomowa1;

/**
 * Homework task 4-11.
 *
 * Specific animal group - Mammals.
 * @author Damir
 */

public abstract class Mammals extends Animals {
    /**
     * Mammals colour.
     */
    private String furColor;

    //private fields available by Getters and Setters

    public String getFurColor() {
        return furColor;
    }

    public void setFurColor(String furColor) {
        this.furColor = furColor;
    }
}
