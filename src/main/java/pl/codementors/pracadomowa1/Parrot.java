package pl.codementors.pracadomowa1;

/**
 * Homework task 4-11.
 *
 * Parrot Class.
 * @author Damir
 */

public class Parrot extends Birds implements Herbivore{
    //Override method from Animals Class
    @Override
    public void eating() {
        eatPlants();

    }
    //Override method from Herbivore Interface
    @Override
    public void eatPlants() {
        System.out.println(getName() + " eating asparagus");

    }

    //standard output/name of animal and onfo what he is doing
    public void tweeting(){
        System.out.println(getName() + " tweet tweet");
    }
}
