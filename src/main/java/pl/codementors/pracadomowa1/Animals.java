package pl.codementors.pracadomowa1;

import java.io.Serializable;

/**
 * Homework task 4-11.
 * <p>
 * Animal representation in abstract class.
 *
 * @author Damir
 * <p>
 * Implement Serializable for saving in binary and reading from binary file.
 */
public abstract class Animals implements Serializable {

    /**
     * Animal name.
     */
    private String name;

    /**
     * Animal age.
     */
    private int age;

    //private fields (age & name) available by Getters and Setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    // void abstract metod
    public abstract void eating();
}
