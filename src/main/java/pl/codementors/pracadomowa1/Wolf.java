package pl.codementors.pracadomowa1;

/**
 * Homework task 4-11.
 * <p>
 * Wolf Class.
 *
 * @author Damir
 */

public class Wolf extends Mammals implements Carnivore {

    //Override method from Animal Class
    @Override
    public void eating() {
        eatMeat();
    }

    //Override method from Carnivore Interface

    @Override
    public void eatMeat() {
        System.out.println(getName() + " eating dear");

    }

    //standard output/name of animal and info what he is doing
    public void howling() {
        System.out.println(getName() + " howl howl");

    }

}
