package pl.codementors.pracadomowa1;

/**
 * Homework task 4-11.
 * <p>
 * Specific animal group - Birds.
 *
 * @author Damir
 */

public abstract class Birds extends Animals {
    /**
     * Birds colour.
     */
    private String colourPlumage;

    //private fields available by Getters and Setters

    public String getColourPlumage() {
        return colourPlumage;
    }

    public void setColourPlumage(String colourPlumage) {
        this.colourPlumage = colourPlumage;
    }
}
