package pl.codementors.pracadomowa1;

/**
 * Homework task 4-11.
 * <p>
 * Iguana Class.
 *
 * @author Damir
 */
public class Iguana extends Reptiles implements Herbivore {

    //Override method from Animals Class
    @Override
    public void eating() {
        eatPlants();

    }

    //Override method from Herbivore Interface
    @Override
    public void eatPlants() {
        System.out.println(getName() + " eating fruits");

    }

    //standard output/name of animal and onfo what he is doing
    public void hissing() {
        System.out.println(getName() + " hiss hiss");

    }
}
