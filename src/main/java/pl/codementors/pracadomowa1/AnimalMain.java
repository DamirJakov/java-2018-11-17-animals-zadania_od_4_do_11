package pl.codementors.pracadomowa1;

import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

/**
 * Homework task 4-11.
 * <p>
 * AnimalMain Class.
 *
 * @author Damir
 */

public class AnimalMain {
    /**
     * Application main method.
     *
     * @param args Application starting parameters
     */

    public static void main(String[] args) {
        System.out.println("Welcome to the ZOO");
        System.out.println();
        Animals[] animals = getDataFromFile("AnimalsInfo.txt");
        //saving file
        save(animals, "AnimalsInfoNew.txt");
        //printing file
        System.out.println("Now is printing file AnimalsInfoNew.txt:");
        print(animals);
        // feeding all Animals
        System.out.println("Feeding all animals:");
        feedingAllAnimals(animals);
        // feeding carnivore
        System.out.println("Feeding all carnivores:");
        feedingCarnivore(animals);
        // feeding herbivore
        System.out.println("Feeding all herbivore:");
        feedingHerbivore(animals);
        // howling
        howl(animals);
        // hissing
        hiss(animals);
        // tweeting
        tweet(animals);
        // saving animals in binary file
        saveInBinaryFile(animals, "AnimalsInfoBinary.bin");
        // read animals from binary
        Animals[] animalsFromBinaryFile = readBinaryFile("AnimalsInfoBinary.bin");
        System.out.println("Now is printing file AnimalsInfoBinary.bin:");
        print(animalsFromBinaryFile);
    }

    /**
     * Reading file AnimalsInfo.txt
     * Fixed @param providing to file
     * Method @return array of animals
     */
    public static Animals[] getDataFromFile(String urlToFile) {
        try {
            File file = new File(urlToFile);
            Scanner sc = new Scanner(file);
            //reading first line from file
            int numberOfAnimals = Integer.valueOf(sc.nextLine());
            // puting animals into array
            Animals[] animals = new Animals[numberOfAnimals];
            for (int i = 0; i < numberOfAnimals; i++) {
                String animalType = sc.nextLine();
                switch (animalType) {
                    //reading all info for every Wolf from file
                    case "Wolf":
                        Wolf wolf = new Wolf();
                        wolf.setName(sc.nextLine());
                        wolf.setAge(Integer.valueOf(sc.nextLine()));
                        wolf.setFurColor(sc.nextLine());
                        animals[i] = wolf;
                        break;
                    //reading all info for every Parrot from file
                    case "Parrot":
                        Parrot parrot = new Parrot();
                        parrot.setName(sc.nextLine());
                        parrot.setAge(Integer.valueOf(sc.nextLine()));
                        parrot.setColourPlumage(sc.nextLine());
                        animals[i] = parrot;
                        break;
                    //reading all info for every Iguana from file
                    case "Iguana":
                        Iguana iguana = new Iguana();
                        iguana.setName(sc.nextLine());
                        iguana.setAge(Integer.valueOf(sc.nextLine()));
                        iguana.setColourScales(sc.nextLine());
                        animals[i] = iguana;
                        break;
                }

            }
            return animals;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Saving file AnimalsInfoNew.txt
     * Fixed @param providing to file
     */
    public static void save(Animals[] animals, String urlToFle) {
        File file = new File(urlToFle);
        try (FileWriter fw = new FileWriter(file)) {
            fw.write(animals.length + "\n");

            for (int i = 0; i < animals.length; i++) {


                fw.write(animals[i].getName() + "\n");
                fw.write(animals[i].getAge() + "\n");
                fw.write(animals[i].getClass().getSimpleName() + "\n");
                switch (animals[i].getClass().getSimpleName()) {
                    case "Wolf":
                        fw.write(((Wolf) animals[i]).getFurColor() + "\n");

                        break;

                    case "Parrot":
                        fw.write(((Parrot) animals[i]).getColourPlumage() + "\n");
                        break;

                    case "Iguana":
                        fw.write(((Iguana) animals[i]).getColourScales() + "\n");
                        break;
                }

            }

        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

    /**
     * Printing file AnimalsInfoNew.txt
     * Fixed @param void providing to file
     */

    public static void print(Animals[] animals) {
        for (int i = 0; i < animals.length; i++) {
            String name, age, type, color = "";
            name = (animals[i].getName() + "\n");
            age = (animals[i].getAge() + "\n");
            type = (animals[i].getClass().getSimpleName() + "\n");

            switch (animals[i].getClass().getSimpleName()) {
                case "Wolf":
                    color = (((Wolf) animals[i]).getFurColor() + "\n");

                    break;

                case "Parrot":
                    color = (((Parrot) animals[i]).getColourPlumage() + "\n");
                    break;

                case "Iguana":
                    color = (((Iguana) animals[i]).getColourScales() + "\n");
                    break;
            }

            System.out.println("Animal type: " + type + "Animal name: " + name + "Animal age: " + age + "Animal color: " + color);

        }
    }

    /**
     * Feeding all animals
     * Fixed @param using array
     */
    public static void feedingAllAnimals(Animals[] animals) {
        for (int i = 0; i < animals.length; i++) {
            if (animals[i] != null) {
                animals[i].eating();
            }
        }
        System.out.println();

    }

    /**
     * Feeding carnivore
     * Fixed @param using array
     */
    public static void feedingCarnivore(Animals[] animals) {
        for (int i = 0; i < animals.length; i++) {
            if (animals[i] instanceof Carnivore) {
                ((Carnivore) animals[i]).eatMeat();
            }

        }
        System.out.println();

    }

    /**
     * Feeding carnivore
     * Fixed @param using array
     */
    public static void feedingHerbivore(Animals[] animals) {
        for (int i = 0; i < animals.length; i++) {
            if (animals[i] instanceof Herbivore) {
                ((Herbivore) animals[i]).eatPlants();
            }

        }
        System.out.println();

    }

    /**
     * Animals howling
     * Fixed @param using array
     */

    public static void howl(Animals[] animals) {
        for (int i = 0; i < animals.length; i++) {
            if (animals[i] instanceof Wolf) {
                ((Wolf) animals[i]).howling();


            }
        }
        System.out.println();
    }

    /**
     * Animals hissing
     * Fixed @param using array
     */
    public static void hiss(Animals[] animals) {
        for (int i = 0; i < animals.length; i++) {
            if (animals[i] instanceof Iguana) {
                ((Iguana) animals[i]).hissing();


            }
        }
        System.out.println();
    }

    /**
     * Animals tweeting
     * Fixed @param using array
     */
    public static void tweet(Animals[] animals) {
        for (int i = 0; i < animals.length; i++) {
            if (animals[i] instanceof Parrot) {
                ((Parrot) animals[i]).tweeting();

            }
        }
        System.out.println();
    }

    /**
     * Saving in binary file
     * Fixed @param using array
     */

    public static void saveInBinaryFile(Animals[] animals, String urlToFile) {
        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(urlToFile))) {
            outputStream.writeObject(animals);
        } catch (IOException e) {
            System.err.println(e);
        }
    }
    /**
     * Reading in binary file
     * Fixed @param using array
     */

    public static Animals[] readBinaryFile(String urlToFile){
        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(urlToFile))) {
            Animals[] animals = (Animals[]) inputStream.readObject();
            return animals;
        } catch (IOException | ClassNotFoundException e) {
            System.err.println(e);
        }
        return null;
    }


}